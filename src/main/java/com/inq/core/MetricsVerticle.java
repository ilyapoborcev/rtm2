package com.inq.core;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.shareddata.LocalMap;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;

import java.util.Collection;

public class MetricsVerticle extends AbstractVerticle {

    private LocalMap<Long, JsonObject> messages;

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        messages = vertx.sharedData().getLocalMap("messages");

        if (messages == null) {
            System.out.println("Error getting local map: null");
            startFuture.fail(new NullPointerException("Error getting local map: null"));
        }

        Router router = Router.router(vertx);
        router
                .route("/realtimemetrics/metrics/live")
                .handler(this::liveMetrics);
        router.exceptionHandler(Throwable::printStackTrace);

        vertx.createHttpServer(
                new HttpServerOptions()
                        .setPort(9999)
                        .setTcpFastOpen(true)
                        .setTcpKeepAlive(true)
                        .setReuseAddress(true)
                        .setReusePort(true)
                        .setTcpCork(true)
                        .setTcpNoDelay(true)
                        .setTcpQuickAck(true)
        )
                .requestHandler(router::accept)
                .rxListen()
                .subscribe(
                        success -> System.out.println("Created HTTP server. Port: " + success.actualPort()),
                        error -> System.out.println("Failed creating HTTP server: " + error.getMessage())
                );
        vertx.exceptionHandler(Throwable::printStackTrace);
    }

    private void liveMetrics(RoutingContext context) {
        System.out.println(Thread.currentThread() + " is handling the request");
        String category = context.request().getParam("category");
        if (category != null) {
            context.response().end(category);
        } else {
            context.put("start time", System.currentTimeMillis());
            context.response()
                    .setChunked(true)
                    .setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("Content-Type", "json")
                    .putHeader("charset", "UTF-8")
                    .write("[\n");

            ((Collection<JsonObject>) messages.getDelegate().values())
                    .forEach(message -> context.response().write(message.encodePrettily()));
            context.response().end("]\n");
        }
    }

}