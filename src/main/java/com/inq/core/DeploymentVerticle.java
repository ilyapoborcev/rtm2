package com.inq.core;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.VertxOptions;
import io.vertx.reactivex.core.Vertx;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

public class DeploymentVerticle {

    public static void main(String... args) {
//        deployClusteredVertx();
        deployVertx();
    }

    private static void deployClusteredVertx() {
        Vertx.rxClusteredVertx(
                new VertxOptions()
                        .setClusterManager(new HazelcastClusterManager())
                        .setClustered(true)
                        .setHAEnabled(false)
        ).subscribe(
                vertx -> {
                    deployMainVerticle(vertx);
                    deployTranscriptUpdateVerticle(vertx);
                },
                error -> System.out.println("Error creating vertx: " + error.getCause())
        );
    }

    private static void deployVertx() {
        Vertx vertx = Vertx.vertx(new VertxOptions());
        deployMetricsVerticle(vertx);
        deployTranscriptUpdateVerticle(vertx);
    }

    private static void deployMetricsVerticle(Vertx vertx) {
        vertx
                .rxDeployVerticle(
                        "com.inq.core.MetricsVerticle",
                        new DeploymentOptions().setInstances(5)
                )
                .subscribe(
                        success -> System.out.println("Successfully deployed MetricsVerticle."),
                        error -> System.out.println("Error during deployment MetricsVerticle: " + error.getCause())
                );
    }

    private static void deployTranscriptUpdateVerticle(Vertx vertx) {
        vertx
                .rxDeployVerticle(
                        "com.inq.core.TranscriptUpdateVerticle",
                        new DeploymentOptions().setInstances(1)
                )
                .subscribe(
                        success -> System.out.println("Successfully deployed TranscriptUpdateVerticle."),
                        error -> System.out.println("Error during deployment TranscriptUpdateVerticle: " + error.getCause())
                );
    }

}
