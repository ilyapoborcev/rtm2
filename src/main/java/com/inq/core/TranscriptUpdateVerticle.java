package com.inq.core;

import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.shareddata.LocalMap;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.BodyHandler;

public class TranscriptUpdateVerticle extends AbstractVerticle {

    private static final String SHARED_MAP_NAME = "messages";
    private LocalMap<Long, JsonObject> messages;
    private long msgId = 0;

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        messages = vertx.sharedData().getLocalMap(SHARED_MAP_NAME);

        if (messages == null) {
            System.out.println("Error getting shared map " + SHARED_MAP_NAME + ": null" );
            startFuture.fail(new NullPointerException("Local map " + SHARED_MAP_NAME + " is not exist"));
        }

        Router router = Router.router(vertx);
        router
                .post("/transcript/transcript_update")
                .handler(BodyHandler.create());
        router
                .post("/transcript/transcript_update")
                .handler(this::transcriptUpdate);
        router.exceptionHandler(Throwable::printStackTrace);

        vertx.createHttpServer(
                new HttpServerOptions()
                        .setPort(10080)
                        .setTcpFastOpen(true)
                        .setTcpKeepAlive(true)
                        .setReuseAddress(true)
                        .setReusePort(true)
                        .setTcpCork(true)
                        .setTcpNoDelay(true)
                        .setTcpQuickAck(true)
        )
                .requestHandler(router::accept)
                .rxListen()
                .subscribe(
                        success -> System.out.println("Created HTTP server. Port: " + success.actualPort()),
                        error -> System.out.println("Failed to create HTTP server: " + error.getMessage())
                );
        vertx.exceptionHandler(Throwable::printStackTrace);
    }

    private void transcriptUpdate(RoutingContext context) {
        System.out.println(Thread.currentThread() + " is handling the request");
        byte[] data = context.getBody().getDelegate().getBytes();

        context
                .response()
                .setStatusCode(200)
                .end();

        JsonObject message = fromBytesData(data);

        if (message != null) {
            messages.put(msgId++, message);
        }
    }

    private JsonObject fromBytesData(final byte[] bytes) {
        if (bytes == null) return null;

        JsonObject message = new JsonObject();
        message.put("text", bytes);

        return message;
    }

}
